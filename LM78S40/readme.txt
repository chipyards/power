backup des simus :
	https://gitlab.com/chipyards/power.git
  <==>	F:\DEV\LTSPICE

Phase 1 : collecte des modeles SPICE disponibles
mc34063all.lib		6 modeles textuels, dont un seul en LTSPICE natif (LTwiki)
MC34063.asy		le symbol pour cette lib
MC33063sch_compact.asc	schema du modele LTwiki, version originale
MC33063sch.asc		schema du modele LTwiki, layout plus lisible
MC33063sch.asy		symbole du modele LTwiki
Note : le modele LTwiki est aussi en texte dans la lib - c'est le meme exactement

buck.asc		simu demo (LTwiki)

Phase 2 : test oscillateur + chacun des 2 feedbacks 
osc_test.asc		seul modele LTwiki fonctionne correctement

Phase 3 : introduire modele LM78S40 : (modele AOP rudimentaire)
buck78.asc		verif du modele 78S40
osc_pwm78.asc		demo oscillateur en mode PWM lineaire
boost78.asc		boost classique (compatible MC33063)
boost78pwm.asc		boost avec PWM lineaire
comparaison boost78.asc vs boost78pwm.asc : le second est moins bruyant a faible puissance mais
son rendement n'est pas meilleur (pourtant les pics de courant y sont bien moindres)

Exploitation :
buck78.asc : devenu specialise pour appli 'reveil projo' 4.5V 30mA
boost78pwm_LED : specialise pour LED 'TV Nico' (ex-boost78pwmMOS)
boost78_LED : le meme sans PWM pour comparaison - il marche bien mais ne permet pas de descendre
la puissance a zero de maniere stable, si on veut un beau dim il faut la PWM
 